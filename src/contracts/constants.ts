/**
 * Constants to be used by models
 */

/**
 * Instruction values
 */
export enum EInstruction {
    FORWARDS = 'F',
    BACKWARDS = 'B',
    LEFT = 'L',
    RIGHT = 'R',
}

/**
 * Bot orientation in the compass
 */
export enum ECompass {
    NORTH = 0,
    EAST = 90,
    SOUTH = 180,
    WEST = 270,
}

/**
 * SatNav instruction values
 */
export enum ESatNav {
    MoveForward = 1,    // Unit
    TurnRight = +90,    // Deg
    TurnLeft = -90,     // Deg
    Reverse = 180,      // Deg
}