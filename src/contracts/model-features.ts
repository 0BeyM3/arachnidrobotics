/**
 * This file contains the feature contracts for bot interfaces
 */
import { ECompass } from './constants';
import { TPosition } from './types';

/**
 * Coordinate contract
 */
export interface ICoordinates {
    x: number,
    y: number,
    getPosition(): TPosition,
}

/**
 * Movement contract.
 * Just because there is a coordinate doesn't mean it can move
 */
export interface IMovement {
    move(instructions: string): TPosition,
}

/**
 * Orientation contract
 */
export interface IOrientation {
    orientation: ECompass | number, // Future support for more than the 4 basic directions
    // Set new orientation based on current + change amount
    getCalculatedOrientation(changeBy: number | ECompass): ECompass,
}

/**
 * Booster contract
 */
export interface IBooster {
    readonly boostLimit: number,
    fuel: number,
    calculatedBoost(units: number): number,
}
