/**
 * Custom types
 */

import { EInstruction } from "./constants";

export type TPosition = [number, number];

export type TInstruction = {
    instruction: EInstruction | string,
    boost: number,
};