import MkBase from './mk-base';
import { EInstruction, ESatNav } from '../contracts/constants';
import { TPosition } from '../contracts/types';

export default class Mk1 extends MkBase {

    move(instructions: string): TPosition {

        MkBase.parseInstructions(instructions).forEach(parsed => {

            // Basic movement
            switch (parsed.instruction) {
                case EInstruction.FORWARDS:
                    this.y += ESatNav.MoveForward;
                    break;
                case EInstruction.BACKWARDS:
                    this.y -= ESatNav.MoveForward;
                    break;
                case EInstruction.LEFT:
                    this.x -= ESatNav.MoveForward;
                    break;
                case EInstruction.RIGHT:
                    this.x += ESatNav.MoveForward;
                    break;
                default:
                    // Unknown instruction
                    break;
            }
        });

        return this.getPosition();
    }
}
