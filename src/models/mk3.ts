import MkBase from './mk-base';
import { EInstruction, ESatNav, ECompass } from '../contracts/constants';
import { IBooster } from '../contracts/model-features';
import { TPosition } from '../contracts/types';

export default class Mk3 extends MkBase implements IBooster {

    // IBooster interface
    readonly boostLimit: number = 5; // TS interfaces don't seem to play nice with 'static'
    fuel: number = 30;


    calculatedBoost(units: number): number {
        // No fuel or not enough fuel to boost far enough.
        // Don't let it land in unwanted places mid boost.
        if (this.fuel <= 0 || this.fuel < units) {
            return 0;
        }

        // Enforce boost limit in units
        if (units > this.boostLimit) {
            units = this.boostLimit;
        }

        // Number of spaces moved
        return units;
    }


    // Abstract implementation. State changes should happen here
    move(instructions: string): TPosition {

        MkBase.parseInstructions(instructions).forEach(parsed => {

            switch (parsed.instruction) {

                // Basic movement
                case EInstruction.FORWARDS:

                    const boostedBy: number = this.calculatedBoost(parsed.boost);

                    // Decrease fuel
                    this.fuel -= boostedBy;

                    // Complex movment: depends on orientation
                    switch (this.orientation) {
                        case ECompass.NORTH:
                            // Attempt to move forward by boost units. Minimum of 1.
                            // Will go nowhere if there is not enough fuel.
                            this.y += boostedBy;
                            break;

                        case ECompass.EAST:
                            this.x += boostedBy;
                            break;

                        case ECompass.SOUTH:
                            this.y -= boostedBy;
                            break;
                        case ECompass.WEST:
                            this.x -= boostedBy;
                            break;

                        default:
                            // Unknown orientation
                            break;
                    }

                    break;

                case EInstruction.BACKWARDS:
                    // Flip orientation
                    this.orientation = this.getCalculatedOrientation(ESatNav.Reverse);
                    break;

                case EInstruction.LEFT:
                    // Turn -90deg
                    this.orientation = this.getCalculatedOrientation(ESatNav.TurnLeft);
                    break;

                case EInstruction.RIGHT:
                    // Turn +90deg
                    this.orientation = this.getCalculatedOrientation(ESatNav.TurnRight);
                    break;

                default:
                    // Unknown instruction
                    break;
            }
        });

        return this.getPosition();
    }
}
