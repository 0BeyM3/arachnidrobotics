import MkBase from './mk-base';
import { EInstruction, ESatNav, ECompass } from '../contracts/constants';
import { TPosition } from '../contracts/types';

export default class Mk2 extends MkBase {

    // Abstract implementation. State changes should happen here
    move(instructions: string): TPosition {

        MkBase.parseInstructions(instructions).forEach(parsed => {

            switch (parsed.instruction) {

                case EInstruction.FORWARDS:
                    // Alter movement depending on orientation
                    switch (this.orientation) {
                        case ECompass.NORTH:
                            this.y += ESatNav.MoveForward;
                            break;

                        case ECompass.EAST:
                            this.x += ESatNav.MoveForward;
                            break;

                        case ECompass.SOUTH:
                            this.y -= ESatNav.MoveForward;
                            break;
                        case ECompass.WEST:
                            this.x -= ESatNav.MoveForward;
                            break;

                        default:
                            // Unknown orientation
                            break;
                    }

                    // Turn around if required
                    if (this.x < 0 || this.y < 0) {
                        // Flip orientation - same as case EInstruction.BACKWARDS
                        this.orientation = this.getCalculatedOrientation(ESatNav.Reverse);

                        // Ensure grid position are always positive
                        this.x = Math.abs(this.x);
                        this.y = Math.abs(this.y);
                    }

                    // This is one long switch statement
                    break;

                case EInstruction.BACKWARDS:
                    // Flip orientation
                    this.orientation = this.getCalculatedOrientation(ESatNav.Reverse);
                    break;

                case EInstruction.LEFT:
                    // Turn -90deg
                    this.orientation = this.getCalculatedOrientation(ESatNav.TurnLeft);
                    break;

                case EInstruction.RIGHT:
                    // Turn +90deg
                    this.orientation = this.getCalculatedOrientation(ESatNav.TurnRight);
                    break;

                default:
                    // Unknown instruction
                    break;
            }
        });

        return this.getPosition();
    }
}
