import { ECompass, EInstruction, ESatNav } from '../contracts/constants';
import { TPosition, TInstruction } from '../contracts/types';
import { ICoordinates, IMovement, IOrientation } from '../contracts/model-features'


/**
 * Base model model
 */
export default abstract class MkBase implements ICoordinates, IMovement, IOrientation {
    #x: number;
    #y: number;
    #orientation: ECompass;

    /**
     * Base model constructor
     * @param {number} startX starting x point
     * @param {number} startY starting y point
     * @param {string} startingInstructions initial instruction set
     * @param startingOrientation 
     */
    constructor(startX: number = 0, startY: number = 0, startingInstructions?: string, startingOrientation?: ECompass) {
        this.#x = startX;
        this.#y = startY;
        this.#orientation = startingOrientation ?? ECompass.NORTH;

        // If supplied, process the instructions
        if (startingInstructions) {
            this.move(startingInstructions);
        }
    }

    /**
     * Extract instructions and boost (if present)
     * @param {string} instructions 
     * @returns {TInstruction[]} array of instruction objects
     */
    static parseInstructions(instructions: string): TInstruction[] {
        const parsed: TInstruction[] = [];

        // Instruction regex
        // group 1: boost
        // group 2: instruction
        const re = /(\d*)(\w{1})/g;

        // Extract instruction and boost
        let result: RegExpExecArray | null = null;
        while (result = re.exec(instructions.toUpperCase())) {

            // If moving forward then boost should be 1 at minimum
            let boost = 0;
            if (result[2] === EInstruction.FORWARDS) {
                // If there is no number then the regex returns an empty string
                // Bitwise Not to return 0 instead of parseInt NaN
                const parsedInt: number = ~~result[1];
                boost = parsedInt > 0 ? parsedInt : ESatNav.MoveForward
            }

            // New instruction
            parsed.push({
                instruction: result[2],
                boost
            });
        }

        return parsed;
    }

    // ICoordinates interface
    set x(newX: number) { this.#x = newX; }

    get x() { return this.#x; }

    set y(newy: number) { this.#y = newy; }

    get y() { return this.#y; }

    /**
     * Get current position
     * @returns {TPosition} current [x, y] coordinates
     */
    getPosition(): TPosition {
        return [this.#x, this.#y];
    }

    // IOrientation interface
    set orientation(newOrientation: ECompass) { this.#orientation = newOrientation };

    get orientation(): ECompass { return this.#orientation; }

    /**
     * Calculate compass direction. Always returns values between 0 and 359. Does not set values
     * @param {number|ECompass} changeBy calculate new coordinate by N degrees
     * @returns {number} new coordinate
     */
    getCalculatedOrientation(changeBy: number | ECompass = 0): number {
        return (this.orientation + Math.abs(360 + changeBy)) % 360;
    }

    // Abstracts
    /**
     * Move bot using satnav instructions. To be implemented on each bot.
     * State changes should happen here.
     * @param {string} instructions SatNav instructions
     */
    abstract move(instructions: string): TPosition;

}
