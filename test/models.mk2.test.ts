import { describe } from 'mocha';
import { expect } from 'chai';
import Mk2 from '../src/models/mk2';
import { ECompass, EInstruction } from '../src/contracts/constants';



describe("Mk2 model", () => {
    describe('Methods', () => {
        describe("move()", () => {


            it('EInstruction.FORWARDS', () => {
                const bot = new Mk2(0, 0);
                bot.move(EInstruction.FORWARDS);

                expect(bot.x).to.equal(0);
                expect(bot.y).to.equal(1);
                expect(bot.orientation).to.equal(ECompass.NORTH);
            });

            it('EInstruction.BACKWARDS', () => {
                const bot = new Mk2(0, 0);
                bot.move(EInstruction.BACKWARDS);

                expect(bot.x).to.equal(0);
                expect(bot.y).to.equal(0);
                expect(bot.orientation).to.equal(ECompass.SOUTH);
            });

            it('EInstruction.LEFT', () => {
                const bot = new Mk2(0, 0);
                bot.move(EInstruction.LEFT);

                expect(bot.x).to.equal(0);
                expect(bot.y).to.equal(0);
                expect(bot.orientation).to.equal(ECompass.WEST);
            });

            it('EInstruction.RIGHT', () => {
                const bot = new Mk2(0, 0);
                bot.move(EInstruction.RIGHT);

                expect(bot.x).to.equal(0);
                expect(bot.y).to.equal(0);
                expect(bot.orientation).to.equal(ECompass.EAST);
            });

            it('Invalid instruction', () => {
                const bot = new Mk2(0, 0);
                bot.move('* ');

                expect(bot.x).to.equal(0);
                expect(bot.y).to.equal(0);
            });

            it('No instruction', () => {
                const bot = new Mk2(0, 0);
                bot.move('');

                expect(bot.x).to.equal(0);
                expect(bot.y).to.equal(0);
            });

            it("Don't go into negative grid area", () => {
                const bot = new Mk2(0, 0);
                bot.move('LFFRFFB');

                expect(bot.x).to.equal(2);
                expect(bot.y).to.equal(2);
                expect(bot.orientation).to.equal(ECompass.SOUTH);
            });
        });
    });

    describe('Test data', () => {

        const testData = [
            {
                initialPosition: { x: 0, y: 0 },
                instructionSet: 'FRFRFFFFFFFLLLLFFFFFRFFFFLFFLRRF',
                expected: { x: 6, y: 13 }
            },
            {
                initialPosition: { x: 3, y: 6 },
                instructionSet: 'FFFFFFFFRRRRRRRFFFFLLLBBRRRRRLLLLLLLLLRFFF',
                expected: { x: 2, y: 14 }
            },
            {
                initialPosition: { x: 0, y: 7 },
                instructionSet: 'RRRRRRRRFFFFFFFFFFFLLLBBBBBRRRLLLLLFFLR',
                expected: { x: 2, y: 18 }
            }
        ];

        // Dynamically build test cases.
        testData.forEach(({ initialPosition, instructionSet, expected }) => {
            it(`Starting position: (${initialPosition.x}, ${initialPosition.y}). Instruction set: "${instructionSet}". Expected: (${expected.x}, ${expected.y})`, () => {

                const bot = new Mk2(initialPosition.x, initialPosition.y, instructionSet);

                expect(bot.x).to.equal(expected.x);
                expect(bot.y).to.equal(expected.y);
            });
        });
    });
});