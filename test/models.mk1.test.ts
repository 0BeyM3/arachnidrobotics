import { describe } from 'mocha';
import { expect } from 'chai';
import Mk1 from '../src/models/mk1';
import { EInstruction } from '../src/contracts/constants';

describe("Mk1 model", () => {

    describe("Basic Movement", () => {

        it('EInstruction.FORWARDS', () => {
            const bot = new Mk1(0, 0);
            bot.move(EInstruction.FORWARDS);

            expect(bot.x).to.equal(0);
            expect(bot.y).to.equal(1);
        });

        it('EInstruction.BACKWARDS', () => {
            const bot = new Mk1(0, 0);
            bot.move(EInstruction.BACKWARDS);

            expect(bot.x).to.equal(0);
            expect(bot.y).to.equal(-1);
        });

        it('EInstruction.LEFT', () => {
            const bot = new Mk1(0, 0);
            bot.move(EInstruction.LEFT);

            expect(bot.x).to.equal(-1);
            expect(bot.y).to.equal(0);
        });

        it('EInstruction.RIGHT', () => {
            const bot = new Mk1(0, 0);
            bot.move(EInstruction.RIGHT);

            expect(bot.x).to.equal(1);
            expect(bot.y).to.equal(0);
        });

        it('Invalid instruction', () => {
            const bot = new Mk1(0, 0);
            bot.move('* ');

            expect(bot.x).to.equal(0);
            expect(bot.y).to.equal(0);
        });

        it('No instruction', () => {
            const bot = new Mk1(0, 0);
            bot.move('');

            expect(bot.x).to.equal(0);
            expect(bot.y).to.equal(0);
        });
    });

    describe('Test data', () => {

        const testData = [
            {
                initialPosition: { x: 0, y: 0 },
                instructionSet: 'FRFRFFFFFFFLLLLFFFFFRFFFFLFFLRRF',
                expected: { x: -1, y: 21 }  // Manually calculated values
            },
            {
                initialPosition: { x: 3, y: 6 },
                instructionSet: 'FFFFFFFFRRRRRRRFFFFLLLBBRRRRRLLLLLLLLLRFFF',
                expected: { x: 4, y: 19 }  // Manually calculated values
            },
            {
                initialPosition: { x: 0, y: 7 },
                instructionSet: 'RRRRRRRRFFFFFFFFFFFLLLBBBBBRRRLLLLLFFLR',
                expected: { x: 3, y: 15 }  // Manually calculated values
            }
        ];

        // Dynamically build test cases.
        testData.forEach(({ initialPosition, instructionSet, expected }) => {
            it(`Starting position: (${initialPosition.x}, ${initialPosition.y}). Instruction set: "${instructionSet}". Expected: (${expected.x}, ${expected.y})`, () => {

                const bot = new Mk1(initialPosition.x, initialPosition.y, instructionSet);

                expect(bot.x).to.equal(expected.x);
                expect(bot.y).to.equal(expected.y);
            });
        });
    });
});