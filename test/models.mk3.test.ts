import { describe } from 'mocha';
import { expect } from 'chai';
import Mk3 from '../src/models/mk3';
import { ECompass, EInstruction } from '../src/contracts/constants';



describe("Mk3 model", () => {

    describe('Properties', () => {

        it('Defaults boost properties', () => {
            const bot = new Mk3(0, 0);
            expect(bot.fuel).to.equal(30);
            expect(bot.boostLimit).to.equal(5);
        });
    });

    describe('Methods', () => {

        describe('calculateBoost()', () => {
            it('Max single boost (limited to 5)', () => {
                const bot = new Mk3(0, 0);
                expect(bot.calculatedBoost(6)).to.eq(5);
            });

            it('Fuel and location after max single boost', () => {
                const bot = new Mk3(0, 0);
                bot.move('10F')

                expect(bot.fuel).to.eq(25);
                expect(bot.x).to.eq(0);
                expect(bot.y).to.eq(5);
            });

            it('No fuel (capacity is 30)', () => {
                const bot = new Mk3(0, 0);
                bot.move('B5F5F5F5F5F5F4F5F5F5F');

                expect(bot.fuel).to.eq(0);
                expect(bot.x).to.eq(0);
                expect(bot.y).to.eq(-30);
            });
        });

        describe("move()", () => {

            it('EInstruction.FORWARDS', () => {
                const bot = new Mk3(0, 0);
                bot.move(EInstruction.FORWARDS);

                expect(bot.x).to.equal(0);
                expect(bot.y).to.equal(1);
                expect(bot.orientation).to.equal(ECompass.NORTH);
            });

            it('EInstruction.BACKWARDS', () => {
                const bot = new Mk3(0, 0);
                bot.move(EInstruction.BACKWARDS);

                expect(bot.x).to.equal(0);
                expect(bot.y).to.equal(0);
                expect(bot.orientation).to.equal(ECompass.SOUTH);
            });

            it('EInstruction.LEFT', () => {
                const bot = new Mk3(0, 0);
                bot.move(EInstruction.LEFT);

                expect(bot.x).to.equal(0);
                expect(bot.y).to.equal(0);
                expect(bot.orientation).to.equal(ECompass.WEST);
            });

            it('EInstruction.RIGHT', () => {
                const bot = new Mk3(0, 0);
                bot.move(EInstruction.RIGHT);

                expect(bot.x).to.equal(0);
                expect(bot.y).to.equal(0);
                expect(bot.orientation).to.equal(ECompass.EAST);
            });

            it('Invalid instruction', () => {
                const bot = new Mk3(0, 0);
                bot.move('* ');

                expect(bot.x).to.equal(0);
                expect(bot.y).to.equal(0);
            });

            it('No instruction', () => {
                const bot = new Mk3(0, 0);
                bot.move('');

                expect(bot.x).to.equal(0);
                expect(bot.y).to.equal(0);
            });

            it("Go into negative grid area", () => {
                const bot = new Mk3(0, 0);
                bot.move('LFFLFFB');

                expect(bot.x).to.equal(-2);
                expect(bot.y).to.equal(-2);
                expect(bot.orientation).to.equal(ECompass.NORTH);
            });
        });
    });

    describe('Test data', () => {

        const testData = [
            {
                initialPosition: { x: 0, y: 0 },
                instructionSet: 'FFFFFF3FLFFFFFFR5FL',
                expected: { x: -6, y: 14, fuel: 10 }
            },
            {
                initialPosition: { x: 3, y: 6 },
                instructionSet: 'FFFFFFFFRRRRRRRFFFFLLLBBRRRRRLLLLLLLLLRFFF',
                expected: { x: 2, y: 14, fuel: 15 }
            },
            {
                initialPosition: { x: 4, y: 3 },
                instructionSet: 'FFFFFFFF5FRFFFFFF3FRFFFFFFLFFFFF5FFF5FFFFFFFLFFFFF',
                expected: { x: 15, y: 10, fuel: 0 }
            }
        ];

        // Dynamically build test cases.
        testData.forEach(({ initialPosition, instructionSet, expected }) => {
            it(`Starting position: (${initialPosition.x}, ${initialPosition.y}). Instruction set: "${instructionSet}". Expected: (${expected.x}, ${expected.y}), fuel: ${expected.fuel}`, () => {

                const bot = new Mk3(initialPosition.x, initialPosition.y)
                bot.move(instructionSet);

                expect(bot.x).to.equal(expected.x);
                expect(bot.y).to.equal(expected.y);
                expect(bot.fuel).to.equal(expected.fuel);
            });
        });
    });
});