import { describe } from 'mocha';
import { expect } from 'chai';
import MkBase from '../src/models/mk-base';
import { TInstruction, TPosition } from '../src/contracts/types';
import { ECompass, ESatNav } from '../src/contracts/constants';

class MkBaseStub extends MkBase {
    // Stub for testing. Does nothing 
    move(instructions: string): TPosition {
        return this.getPosition();
    }
}

describe("Base model", () => {
    describe('Properties', () => {

        it('Defaults', () => {
            const bot = new MkBaseStub();

            expect(bot.x).to.equal(0);
            expect(bot.y).to.equal(0);
            expect(bot.orientation).to.equal(ECompass.NORTH);
            expect(bot.getPosition()).deep.equal([0, 0]);
        });

        it('Supplied properties', () => {
            const bot = new MkBaseStub(2, 3, 'move method does nothing', ECompass.SOUTH);

            expect(bot.x).to.equal(2);
            expect(bot.y).to.equal(3);
            expect(bot.orientation).to.equal(ECompass.SOUTH);
            expect(bot.getPosition()).deep.equal([2, 3]);
        });

    });


    describe('Methods', () => {

        // Account for ECompass values. Can be changed to support 360deg when a new model supports it.
        describe('getCalculatedOrientation()', () => {

            it('No param', () => {
                const bot = new MkBaseStub(0, 0);
                expect(bot.getCalculatedOrientation()).to.eq(0);
            });

            it('North then turn left', () => {
                const bot = new MkBaseStub(0, 0);
                expect(bot.getCalculatedOrientation(ESatNav.TurnLeft)).to.eq(ECompass.WEST);
            });

            it('North then turn right', () => {
                const bot = new MkBaseStub(0, 0);
                expect(bot.getCalculatedOrientation(ESatNav.TurnRight)).to.eq(ECompass.EAST);
            });

            it('North then reverse', () => {
                const bot = new MkBaseStub(0, 0);
                expect(bot.getCalculatedOrientation(ESatNav.Reverse)).to.eq(ECompass.SOUTH);
            });

            it('East then reverse', () => {
                const bot = new MkBaseStub(0, 0, '', ECompass.EAST);
                expect(bot.getCalculatedOrientation(ESatNav.Reverse)).to.eq(ECompass.WEST);
            });
        });

        describe('parseInstructions()', () => {

            it('Valid instructions', () => {
                const instructions = MkBase.parseInstructions('FLRB');
                const expected: TInstruction[] = [
                    { instruction: 'F', boost: 1 },
                    { instruction: 'L', boost: 0 },
                    { instruction: 'R', boost: 0 },
                    { instruction: 'B', boost: 0 }
                ];

                expect(instructions).deep.equal(expected);
            });

            it('Invalid instructions', () => {
                const instructions = MkBase.parseInstructions('F!*(()  LRB');
                const expected: TInstruction[] = [
                    { instruction: 'F', boost: 1 },
                    { instruction: 'L', boost: 0 },
                    { instruction: 'R', boost: 0 },
                    { instruction: 'B', boost: 0 }
                ];

                expect(instructions).deep.equal(expected);
            });

            it('No instructions', () => {
                const instructions = MkBase.parseInstructions('');
                const expected: TInstruction[] = [];

                expect(instructions).deep.equal(expected);
            });

        });
    });
});