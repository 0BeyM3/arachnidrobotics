# Ticker Technical Test - Arachnid Robotics

## Notes

The project was rebuilt for a more suitable pattern. Now Arachnid Robotics can improve the base model without affecting existing models!

# Unit tests

`npm test`

# Parts

# 1. Mk1

## Assumptions and requirements

- Grid starts at (0, 0)
- No specific programming style or pattern required
- Not enough complexity to completely split the source code
- The project entry point does nothing and code is checked through unit tests
- Invalid instructions are ignored
- Instructions are case-insensitive (all converted to uppercase)

# 2. Mk2

## Api versioning

- Mk1 support is retained
- Versioning is done by expanding on the base model but without to branch inside class

## Instructions

- Left: +90deg rotation
- Right: -90deg rotation
- Backwards: ±180deg rotation
- Forwards: translate by N units in angle

## Constraints

- Instructions leading to negative points instead invert the orientation
  - e.g. (3, 0) facing oritended to 270deg followed by instruction F results in (3, 1) instead of (3, -1)

## Assumptions

- Fowards instruction is equivalent to 1 unit
- Grid has not positive limits aka the only edges are on negative numbers

# 3. Mk3

## Api versioning

- Mk2 support is retained

## Instructions

- Forward instructions can now include boost
  - e.g. 5F boosts forwards by 5
  - e.g. F boosts forwards by 1

## Constraints

- Can go into negative grid zones
- Cannot boost more than 5 units

## Assumptions

- Trying to boost further than fuel can cover will result in not boosting at all. We don't want it to be stranded
  - i.e. trying to boost 3 when only 2 fuel units are available will not move the bot
- Once fuel runs out the unit can no longer move forward
